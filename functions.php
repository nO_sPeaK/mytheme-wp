<?php
function load_css() {
    wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), 1 , 'all');
    wp_enqueue_style('bootstrap');
}
add_action( 'wp_enqueue_scripts', 'load_css' );




function load_js() {
    wp_register_script('my_jquery', get_template_directory_uri() . '/js/jquery-3.5.1.slim.min.js', array(), 1 , 1 , 1);
    wp_enqueue_script('my_jquery');

    wp_register_script('popper', get_template_directory_uri() . '/js/popper.min.js', array(), 1 , 1 , 1);
    wp_enqueue_script('popper');

    wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), 1 , 1 , 1);
    wp_enqueue_script('bootstrap');
}
add_action( 'wp_enqueue_scripts', 'load_js' );
?>