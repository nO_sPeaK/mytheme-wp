<!doctype html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?> ">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php wp_title();?> | <?php bloginfo('name');?></title>
    <?php wp_head();?>
</head>
<body <?php body_class();?>>
<header>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">خانه</a>
    </nav>
</header>

<main>
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                    <p class="card-text"><?php the_excerpt();?></p>
                    <p class="card-text"><small class="text-muted"><?php echo get_the_date()  ?> | <?php the_author_posts_link(); ?></small></p>
                    <p class="card-text"><small class="text-muted"><?php the_category(', ');  ?></small></p>
                </div>
            </div>
    <?php endwhile; else: endif; ?>
</main>

<footer class="text-center">
    <div>this is footer</div>
</footer>

<?php wp_footer();?>
</body>
</html>